'use strict'
var express = require('express');
var app = express();



//Midleware
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept", 'application/json', 'text/json');
  res.header("Access-Control-Allow-Methods", 'POST, GET, PUT, DELETE, OPTIONS');

  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
  next();
});


function Api(){
  'use strict';

  const request = require('request');

  let subscriptionKey = "yourKey";
  let endpoint = "yourEndPoint"
  if (!subscriptionKey) { throw new Error('Set your environment variables for your subscription key and endpoint.'); }

  var uriBase = endpoint + 'vision/v3.0/analyze';

  const imageUrl =
      'https://la.network/wp-content/uploads/2019/09/mario-home.jpg';//Your imagen link

  // Request parameters.
  const params = {
      'visualFeatures': 'Categories,Description,Color',
      'details': '',
      'language': 'es'
  };

  const options = {
      uri: uriBase,
      qs: params,
      body: '{"url": ' + '"' + imageUrl + '"}',
      headers: {
          'Content-Type': 'application/json',
          'Ocp-Apim-Subscription-Key' : subscriptionKey
      }
  };

  request.post(options, (error, response, body) => {
    if (error) {
      console.log('Error: ', error);
      return;
    }
    let jsonResponse = JSON.stringify(JSON.parse(body), null, '  ');
    console.log('JSON Response\n');
    console.log(jsonResponse);
  });
}
Api();
module.exports = app;


